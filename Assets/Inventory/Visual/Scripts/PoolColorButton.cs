﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace ColorInventory
{
    public class PoolColorButton : MonoBehaviour
    {
        #region Global State

        ///<summary>
        /// List of all pool color button
        ///</summary>
        public static List<PoolColorButton> All = new List<PoolColorButton>();
        ///<summary>
        /// Selected pool color button
        ///</summary>
        public static PoolColorButton selectedButton = null;

        #endregion



        #region State

        ///<summary>
        /// Cached UI button component
        ///</summary>
        Button button;

        public Button Button
        {
            get { return button ?? gameObject.GetComponent<Button>(); }
            set { button = value; }
        }

        #endregion State



        #region Methods
        ///<summary>
        /// Select this color
        ///</summary>
        public void Select()
        {

            if (selectedButton != null) selectedButton.Unselect();
            selectedButton = this;
            transform.localScale = Vector3.one * 0.8f;

        }

        ///<summary>
        /// Unselect this color
        ///</summary>
        public void Unselect()
        {
            selectedButton.transform.localScale = Vector3.one;

        }
        #endregion Methods

        #region Unity

        ///<summary>
        /// This function is called when the behaviour becomes enabled and active.
        ///</summary>
        void OnEnable()
        {
            if (selectedButton == null) Select();
            All.Add(this);
            Button.onClick.AddListener(Select);
        }

        ///<summary>
        /// This function is called when the behaviour becomes disabled.
        ///</summary>
        private void OnDisable()
        {
            if (All.Count != 0) All[0].Select();
            Button.onClick.RemoveListener(Select);
            All.Remove(this);
        }

        ///<summary>
        /// This function is called when the behaviour becomes destroy.
        ///</summary>
        private void OnDestroy()
        {
            if (All.Count != 0) All[0].Select();
            Button.onClick.RemoveListener(Select);
            All.Remove(this);
        }

        #endregion Unity
    }
}