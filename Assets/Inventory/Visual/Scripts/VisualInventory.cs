﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ColorInventory
{
    ///<summary>
    /// Visual of inventory
    ///</summary>
    public class VisualInventory : MonoBehaviour
    {
        #region State

        ///<summary>
        /// Cached inventory variable
        ///</summary>
        Inventory inventory;

        Inventory Inventory
        {
            get
            {
                if (inventory == null)
                {
                    if (Inventory.Instance != null) return inventory = Inventory.Instance;
                    else
                    {
                        Debug.Log("Inventory singleton don't exist.");
                        return inventory;
                    }
                }
                else return inventory;
            }
            set { inventory = value; }
        }

        ///<summary>
        /// List of visual inventory color buttons
        ///</summary>
        List<Button> colorsVisual;

        ///<summary>
        /// List of visual pool color buttons
        ///</summary>
        List<Button> poolColorsVisual;



        #endregion State

        #region Parameters

        ///<summary>
        /// Dummy color image prefab
        ///</summary>
        [SerializeField]
        GameObject colorImagePrefab;

        ///<summary>
        /// Parent for visual pool colors
        ///</summary>
        [SerializeField]
        GameObject colorPoolVisualList;

        ///<summary>
        /// Parent for visual inventory colors
        ///</summary>
        [SerializeField]
        GameObject colorInventoryVisualList;

        #endregion Parameters


        #region Methods


        ///<summary>
        /// Add selected color to inventory and visual inventory
        ///</summary>
        public void AddColor()
        {
            Inventory.AddColor(PoolColorButton.selectedButton.Button.targetGraphic.color);
        }

        ///<summary>
        /// Add color to inventory and visual inventory
        ///</summary>
        void AddColor(Color color)
        {
            Button buttonTmp = null;
            buttonTmp = Instantiate(colorImagePrefab, colorInventoryVisualList.transform).GetComponent<Button>();
            buttonTmp.transform.name = "Color #" + ColorUtility.ToHtmlStringRGBA(color);
            buttonTmp.gameObject.AddComponent<InventoryColorButton>();
            colorsVisual.Add(buttonTmp);
            buttonTmp.targetGraphic.color = color;

            foreach (Button b in poolColorsVisual)
            {
                if (b.targetGraphic.color == color)
                {
                    b.interactable = false;
                    break;
                }
            }
        }

        ///<summary>
        /// Delete selected color from inventory and visual inventory
        ///</summary>
        public void DeleteColor()
        {
            if (InventoryColorButton.selectedButton != null)
                Inventory.DeleteColor(InventoryColorButton.selectedButton.Button.targetGraphic.color);
        }

        ///<summary>
        /// Delete color from inventory and visual inventory
        ///</summary>
        void DeleteColor(Color color)
        {
            foreach (Button b in colorsVisual)
            {
                if (b.targetGraphic.color == color)
                {
                    colorsVisual.Remove(b);
                    Destroy(b.gameObject);
                    break;
                }
            }
            foreach (Button b in poolColorsVisual)
            {
                if (b.targetGraphic.color == color)
                {
                    b.interactable = true;
                    break;
                }
            }
        }



        #endregion Methods

        #region Unity


        ///<summary>
        /// Start is called on the frame when a script is enabled just before any of the Update methods are called the first time.
        ///</summary>
        void Start()
        {
            Button buttonTmp = null;
            poolColorsVisual = new List<Button>();
            foreach (Color c in Inventory.ColorPool.availableColors)
            {
                buttonTmp = Instantiate(colorImagePrefab, colorPoolVisualList.transform).GetComponent<Button>();
                buttonTmp.transform.name = "Color #" + ColorUtility.ToHtmlStringRGBA(c);
                buttonTmp.gameObject.AddComponent<PoolColorButton>();
                poolColorsVisual.Add(buttonTmp);
                buttonTmp.targetGraphic.color = c;
            }
            colorsVisual = new List<Button>();
            foreach (Color c in Inventory.Colors)
            {
                buttonTmp = Instantiate(colorImagePrefab, colorInventoryVisualList.transform).GetComponent<Button>();
                buttonTmp.transform.name = "Color #" + ColorUtility.ToHtmlStringRGBA(c);
                buttonTmp.gameObject.AddComponent<InventoryColorButton>();
                colorsVisual.Add(buttonTmp);
                buttonTmp.targetGraphic.color = c;
            }

            foreach (Button b in poolColorsVisual)
            {
                if (Inventory.Instance.Colors.Contains(b.targetGraphic.color)) b.interactable = false;
            }
            Inventory.AddColorEvent += AddColor;
            Inventory.DeleteColorEvent += DeleteColor;
        }

        #endregion Unity
    }
}