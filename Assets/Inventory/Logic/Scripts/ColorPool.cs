﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ColorInventory
{

    [CreateAssetMenu(fileName = "Color pool", menuName = "Inventory/Color pool", order = 1)]
    public class ColorPool : ScriptableObject
    {
        ///<summary>
        /// List of available colors
        ///</summary>
        public List<Color> availableColors;
    }



}