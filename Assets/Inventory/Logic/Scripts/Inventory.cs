﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace ColorInventory
{

    public class Inventory : MonoBehaviour
    {
        #region Global State

        ///<summary>
        /// Instance of singleton
        ///</summary>
        public static Inventory Instance = null;

        #endregion Global State

        #region State

        ///<summary>
        /// List of colors in inventory
        ///</summary>
        [SerializeField]
        ColorsList colors;

        public List<Color> Colors
        {
            get
            {
                if (colors != null)
                    return new List<Color>(colors.colors);
                else return new List<Color>();
            }
            private set { colors.colors = value; }
        }

        #endregion

        #region Parameters

        ///<summary>
        /// Pool color that can be added in inventory
        ///</summary>
        [SerializeField]
        ColorPool colorPool;

        public ColorPool ColorPool
        {
            get { return colorPool; }
            private set { colorPool = value; }
        }
        #endregion


        #region Events


        public delegate void ChangeInventory(Color color);

        ///<summary>
        /// Event called when color added
        ///</summary>
        public event ChangeInventory AddColorEvent;
        ///<summary>
        /// Event called when color deleted
        ///</summary>
        public event ChangeInventory DeleteColorEvent;


        #endregion

        #region Methods


        ///<summary>
        /// Add color to inventory
        ///</summary>
        public void AddColor(Color color)
        {
            if (colorPool != null)
            {

                if (colorPool.availableColors.Contains(color))
                {
                    if (colors.colors.Contains(color))
                    {
                        Debug.Log("Try add existing color.");
                    }
                    else
                    {
                        colors.colors.Add(color);
                        PlayerPrefs.SetString("Inventory.Colors", JsonUtility.ToJson(colors));
                        PlayerPrefs.Save();
                        if (AddColorEvent != null) AddColorEvent(color);
                    }
                }
                else
                {
                    Debug.Log("Try add not available color.");
                }

            }
        }

        ///<summary>
        /// Delete color from inventory
        ///</summary>
        public void DeleteColor(Color color)
        {
            if (colors.colors.Contains(color))
            {
                colors.colors.Remove(color);
                PlayerPrefs.SetString("Inventory.Colors", JsonUtility.ToJson(colors));
                PlayerPrefs.Save();
                if (DeleteColorEvent != null) DeleteColorEvent(color);
            }
            else
            {
                Debug.Log("Try delete not contained color");
            }
        }

        #endregion

        #region Unity

        ///<summary>
        /// Call one time after activate object
        ///</summary>
        void Awake()
        {
            if (Instance == null) Instance = this;
            Debug.Log(PlayerPrefs.GetString("Inventory.Colors"));
            colors = JsonUtility.FromJson<ColorsList>(PlayerPrefs.GetString("Inventory.Colors"));
            if (colors == null) colors.colors = new List<Color>();
        }



        #endregion
    }

    ///<summary>
    /// Color list for serialize in JSON string
    ///</summary>
    [System.Serializable]
    class ColorsList
    {
        [SerializeField]
        public List<Color> colors = new List<Color>();
    }

}
